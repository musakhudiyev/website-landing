import '../scss/main.scss';
import menu from './modules/menu';
import animations from "./modules/animations";
import diagram from "./modules/diagram";
import tooltips from "./modules/tooltips";

window.addEventListener('DOMContentLoaded', () => {
  'use strict';
  menu();
  animations();
  tooltips();
  // diagram();
})
