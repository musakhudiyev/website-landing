import '../scss/main.scss';
import animations from './modules/animations';
import sliders from './modules/sliders';
import tabs from './modules/tabs';
import menu from './modules/menu';
import tooltips from "./modules/tooltips";

window.addEventListener('DOMContentLoaded', () => {
  'use strict';
  menu();
  sliders();
  tooltips();
  animations();
  tabs('.profit-tabs', '.profit-tabs__nav-item', '.profit-tabs__content-item');
  tabs('.roadmap-tabs', '.roadmap-tabs__nav-item', '.roadmap-tabs__content-item');
})
