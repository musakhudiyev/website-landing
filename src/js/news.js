import '../scss/main.scss';
import menu from './modules/menu';

window.addEventListener('DOMContentLoaded', () => {
  'use strict';
  menu();
})
