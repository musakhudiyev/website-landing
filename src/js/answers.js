import '../scss/main.scss';
import tabs from './modules/tabs';
import menu from './modules/menu';
import accordion from "./modules/accordion";

window.addEventListener('DOMContentLoaded', () => {
  'use strict';
  menu();
  tabs('.answers-tabs', '.answers-tabs__nav-item', '.answers-tabs__content-item');
  accordion();
})
