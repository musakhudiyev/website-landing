const accordion = () => {
  let accordionButtons = document.querySelectorAll(".accordion__button");

  for (let i = 0; i < accordionButtons.length; i++) {
    let accordionButton = accordionButtons[i];

    accordionButton.addEventListener("click", toggleItems);
  }

  function toggleItems() {

    let itemClass = this.className;

    for (let i = 0; i < accordionButtons.length; i++) {
      accordionButtons[i].className = "accordion__button closed";
    }

    let panels = document.querySelectorAll(".accordion__panel");
    for (let z = 0; z < panels.length; z++) {
      panels[z].style.maxHeight = 0;
    }

    if (itemClass === "accordion__button closed") {
      this.className = "accordion__button active";
      let panel = this.nextElementSibling;
      panel.style.maxHeight = panel.scrollHeight + "px";
    }

  }
}

export default accordion;
