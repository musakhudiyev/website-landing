const menu = () => {
  const menuBtn = document.querySelector('.menu-btn');
  const navBar = document.querySelector('.navbar');

  menuBtn.addEventListener('click', () => {

    if (navBar.classList.contains('active')) {
      closeMenu()
    } else {
      openMenu()
    }

  })

  function openMenu() {
    menuBtn.classList.add('active');
    navBar.classList.add('active');
    document.body.style.overflow = 'hidden';
  }

  function closeMenu() {
    menuBtn.classList.remove('active');
    navBar.classList.remove('active');
    document.body.style.overflow = '';
  }
}

export default menu;
