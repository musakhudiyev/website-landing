const diagram = () => {
  const triggers = document.querySelectorAll('.directions__diagram-path');
  const tooltips = document.querySelectorAll('.directions__diagram-info');

  triggers.forEach(trigger => {
    tooltips.forEach(tooltip => {
      trigger.addEventListener('mousemove', (e) => {
        if (e.target.getAttribute('data-id') === tooltip.id ) {
          showTooltip(e)
        }
      })

      trigger.addEventListener('mouseout', (e) => {
        if (e.target.getAttribute('data-id') === tooltip.id ) {
          hideTooltip()
        }
      })

    })
  })

  function showTooltip(event) {
    tooltips.forEach(tooltip => {
      tooltip.classList.add('show')
      tooltip.style.left = event.pageX + 10 + 'px';
      tooltip.style.top = event.pageY + 10 + 'px';
    })
  }

  function hideTooltip() {
    tooltips.forEach(tooltip => {
      tooltip.classList.remove('show')
    })
  }
}

export default diagram;
