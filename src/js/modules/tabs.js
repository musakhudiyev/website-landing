const tabs = (parentSelector, tabSelector, contentSelector) => {
  const tabsParent = document.querySelector(parentSelector);
  const tabs = document.querySelectorAll(tabSelector);
  const tabsContent = document.querySelectorAll(contentSelector);

  const hideTabsContent = () => {
    tabsContent.forEach(item => {
      item.classList.remove('show');
      item.classList.add('hide');
    })

    tabs.forEach(item => {
      item.classList.remove('active');
    })
  }

  const showTabsContent = (i = 0) => {
    tabsContent[i].classList.remove('hide')
    tabsContent[i].classList.add('show');
    tabs[i].classList.add('active');
  }

  hideTabsContent();
  showTabsContent();

  tabsParent.addEventListener('click', (e) => {
    const target = e.target;

    if (target && target.closest('.tabs__nav-item')) {
      tabs.forEach((item, i) => {

        if (target === item || target.parentNode === item) {
          item.classList.add('active')
          hideTabsContent();
          showTabsContent(i);
        }
      })
    }
  })
}

export default tabs;


