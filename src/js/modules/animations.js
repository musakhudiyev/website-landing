const animations = () => {
  function Animator({
                      canvas,
                      images
                    }) {
    const ctx = canvas.getContext('2d')
    let index = 0, fpsInterval, startTime, now, then, elapsed;

    this.start = function(fps = 25) {
      fpsInterval = 1000 / fps;
      then = performance.now();
      startTime = then;
      animate2(true);
    }

    function animate2(forward = true) {
      if (forward) {
        if (index < images.length) {
          requestAnimationFrame(() => animate2(true));
          now = performance.now();
          elapsed = now - then;
          if (elapsed > fpsInterval) {
            then = now - (elapsed % fpsInterval);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(images[index], 1020, 100, 800, 850, 0, 0, 800, 800)
            index++
          }
        } else {
          requestAnimationFrame(() => animate2(false));
          now = performance.now();
          elapsed = now - then;
        }
      } else {
        if (index > 0) {
          requestAnimationFrame(() => animate2(false));
          now = performance.now();
          elapsed = now - then;
          if (elapsed > fpsInterval) {
            then = now - (elapsed % fpsInterval);
            index--
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(images[index], 1020, 100, 800, 850, 0, 0, 800, 800)
          }
        } else {
          requestAnimationFrame(() => animate2(true));
          now = performance.now();
          elapsed = now - then;
        }
      }
    }
  }

  !(function() {
    const canvas = document.getElementById('canvas_1')
    canvas.width = 800
    canvas.height = 820
    // const ctx = canvas.getContext('2d')
    const images = []

    for (let i = 1; i < 91; i++) {
      images.push(`img/intro/demoFF200${i < 10 ? '0' : ''}${i}.png`)
    }

    images.forEach((item, index, array) => {
      let img = new Image()
      img.src = item
      img.onload = function() {
        images[index] = img
        if (index === array.length - 1) {
          const anim = new Animator({
            canvas,
            images
          })
          anim.start()
        }
      }
    })
  })();

  !(function() {
    const canvas = document.getElementById('canvas_2')
    canvas.width = 800
    canvas.height = 800
    // const ctx = canvas.getContext('2d')
    const images = []

    for (let i = 1; i < 146; i++) {
      images.push(`img/sphere/00${i < 10 ? '00' : i < 100 ? '0' : ''}${i}.png`)
    }

    images.forEach((item, index, array) => {
      let img = new Image()
      img.src = item
      img.onload = function() {
        images[index] = img
        if (index === array.length - 1) {
          const anim = new Animator({
            canvas,
            images
          })
          anim.start()
        }
      }
    })
  })();
}

export default animations;
