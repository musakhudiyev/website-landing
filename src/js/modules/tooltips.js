const tooltips = () => {
  const triggers = document.querySelectorAll('.tooltip-trigger');
  const tooltips = document.querySelectorAll('.tooltip');

  triggers.forEach(trigger => {
    tooltips.forEach(tooltip => {
      trigger.addEventListener('mousemove', (e) => {
        if (e.target.getAttribute('data-id') === tooltip.id ) {
          showTooltip(e)
        }
      })

      trigger.addEventListener('mouseout', (e) => {
        if (e.target.getAttribute('data-id') === tooltip.id ) {
          hideTooltip()
        }
      })

    })
  })

  function showTooltip(event) {
    tooltips.forEach(tooltip => {
      tooltip.classList.add('show')
      tooltip.style.left = event.pageX + 10 + 'px';
      tooltip.style.top = event.pageY + 10 + 'px';
    })
  }

  function hideTooltip() {
    tooltips.forEach(tooltip => {
      tooltip.classList.remove('show')
    })
  }
}

export default tooltips;
