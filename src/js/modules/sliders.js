import Swiper, {Navigation, Pagination, Autoplay, EffectFade} from 'swiper';
Swiper.use([Navigation, Pagination, Autoplay, EffectFade]);
import 'swiper/css/bundle';

const sliders = () => {

  new Swiper('.why__slider.swiper', {
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
    autoplay: {
      delay: 2500,
      disableOnInteraction: false,
    },
    speed: 1000,
  });

  new Swiper('.buy__slider.swiper', {
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
    autoplay: {
      delay: 2500,
      disableOnInteraction: false,
    },
    speed: 1000,
  });

  new Swiper('#slider-01.swiper', {
    slidesPerView: 5,
    spaceBetween: 45,
    speed: 1000,
    observer: true,
    observeParents: true,
    navigation: {
      nextEl: '#arrows-01 .swiper-button-next',
      prevEl: '#arrows-01 .swiper-button-prev',
    },
    breakpoints: {
      320: {
        slidesPerView: 2,
        spaceBetween: 10,
      },
      576: {
        slidesPerView: 3,
        spaceBetween: 30
      },
      992: {
        slidesPerView: 3,
        spaceBetween: 45
      },
      1200: {
        slidesPerView: 4,
        spaceBetween: 45
      },
      1600: {
        slidesPerView: 5,
        spaceBetween: 45
      }
    }
  });

  new Swiper('#slider-02.swiper', {
    slidesPerView: 5,
    spaceBetween: 45,
    speed: 1000,
    observer: true,
    observeParents: true,
    navigation: {
      nextEl: '#arrows-02 .swiper-button-next',
      prevEl: '#arrows-02 .swiper-button-prev',
    },
    breakpoints: {
      320: {
        slidesPerView: 2,
        spaceBetween: 10,
      },
      576: {
        slidesPerView: 3,
        spaceBetween: 30
      },
      992: {
        slidesPerView: 3,
        spaceBetween: 45
      },
      1200: {
        slidesPerView: 4,
        spaceBetween: 45
      },
      1600: {
        slidesPerView: 5,
        spaceBetween: 45
      }
    }
  });
}

export default sliders;



