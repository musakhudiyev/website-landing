/* eslint-disable no-undef */
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const CopyWebpackPlugin = require("copy-webpack-plugin");
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: {
    main: path.resolve(__dirname, './src/js/main.js'),
    news: path.resolve(__dirname, './src/js/news.js'),
    article: path.resolve(__dirname, './src/js/article.js'),
    answers: path.resolve(__dirname, './src/js/answers.js'),
    details: path.resolve(__dirname, './src/js/details.js'),
  },
  output: {
    path: path.resolve(__dirname, './public'),
    filename: '[name].bundle.js',
    clean: true
  },
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  },
  devtool: 'source-map',
  devServer: {
    historyApiFallback: true,
    contentBase: path.resolve(__dirname, './public'),
    open: true,
    compress: true,
    hot: true,
    port: 8080,
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve(__dirname, './src/index.html'),
      inject: 'body',
      chunks: ['main'],
    }),
    new HtmlWebpackPlugin({
      filename: 'news.html',
      template: path.resolve(__dirname, './src/pages/news.html'),
      inject: 'body',
      chunks: ['news'],
    }),
    new HtmlWebpackPlugin({
      filename: 'article.html',
      template: path.resolve(__dirname, './src/pages/article.html'),
      inject: 'body',
      chunks: ['article'],
    }),
    new HtmlWebpackPlugin({
      filename: 'answers.html',
      template: path.resolve(__dirname, './src/pages/answers.html'),
      inject: 'body',
      chunks: ['answers'],
    }),
    new HtmlWebpackPlugin({
      filename: 'details.html',
      template: path.resolve(__dirname, './src/pages/details.html'),
      inject: 'body',
      chunks: ['details'],
    }),
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin({
      patterns: [
        {from: './src/assets', to: './'},
        {from: './src/scss', to: 'scss'},
        {from: './src/*.html', to: './'},
      ],
    }),
    new SVGSpritemapPlugin('./src/assets/svg/**/*.svg', {
      output: {
        filename: 'sprite.svg',
      },
      sprite: {
        prefix: false,
      }
    }),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
      },
      {
        test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
        type: 'asset/inline',
      },
      {
        test: /\.(scss|css)$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
    ],
  }
}
